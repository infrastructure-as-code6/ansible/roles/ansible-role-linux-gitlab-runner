Role: linux-gitlab-runner
=========
A brief description of the role goes :

This role create `linux` gitlab runners for cicd pipelines.

Requirements :pushpin:
------------
Pre-requisites that may not be covered by Ansible : 

- **python3** (master node and managed node)

Role Variables :wrench:
--------------

```yml
#default values
runner_qty: 2
runner_conccurency : 2
runner_template_location: /etc/gitlab-runner/runner.template.toml
runner_config : 
    description: docker-runner
    url: https://gitlab.com/
    docker_image: docker:stable
    run_untagged: true
    locked: false
    access_level: not_protected
    registration_token: "{{ vault_runner_registration_token  }}"
    tag_list: "docker,all,dind"
    privileged: true
    limit: 0
```


Dependencies :paperclip:
------------
A list of other roles should go here and any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles :

- **role** : [gitlab-runner-install:>=v1.*](https://github.com/enzo-cora/ansible-role-gitlab-runner-install)
- **role** : [docker:>=4.2.*](https://galaxy.ansible.com/geerlingguy/docker)

Example Playbook :clapper:
----------------

```yml 
- name : pre_task installation
  gather_facts: true
  become: true
  hosts:
    - all
  roles:
    - docker
    - gitlab-runner-install
  tags:
    - installation

- name : Register gitlab-runners
  become: true
  gather_facts: false
  hosts:
    - all
  roles:
    - gitlab-runner     #<----- gitlab-runner role 
  tags:
    - registration
```
